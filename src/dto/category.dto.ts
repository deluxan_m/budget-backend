import { IsNumber, IsString, MaxLength, MinLength } from "class-validator";

export class CategoryDto {
  @IsNumber()
  user_id: number;

  @IsString()
  @MinLength(3)
  @MaxLength(10)
  name: string;

  @IsString()
  @MinLength(3)
  @MaxLength(12)
  icon: string;

  @IsString()
  @MinLength(4)
  @MaxLength(10)
  color: string;
}
