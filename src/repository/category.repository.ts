import { NotFoundException } from "@nestjs/common";
import { CategoryDto } from "src/dto/category.dto";
import { Category } from "src/entity/category.entity";
import { EntityRepository, Repository } from "typeorm";

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
  // CRUD => CREATE
  async createCategory(categoryDto: CategoryDto): Promise<Category> {
    const { user_id, name, icon, color } = categoryDto;

    const category = new Category();

    category.user_id = user_id;
    category.name = name;
    category.icon = icon;
    category.color = color;

    await category.save();

    return category;
  }

  // CRUD => READ
  async getCategory(id: number): Promise<Category> {
    const category = await this.findOne(id);

    if (!category) {
      throw new NotFoundException("Category not found");
    }

    return category;
  }

  async getCategories(): Promise<Category[]> {
    return await this.find();
  }

  // CRUD => UPDATE
  async updateCategory(
    id: number,
    categoryDto: CategoryDto
  ): Promise<Category> {
    const category = await this.getCategory(id);

    const { name, icon, color } = categoryDto;

    category.name = name;
    category.icon = icon;
    category.color = color;

    this.update(id, category);

    return category;
  }

  // CRUD => DELETE
  async deleteCategory(id: number): Promise<string> {
    const result = await this.getCategory(id);

    if (!result) {
      throw new NotFoundException("Data not found");
    }

    await this.delete(id);

    return "Successfully deleted";
  }
}
