import { Injectable } from "@nestjs/common";
import { CategoryDto } from "src/dto/category.dto";
import { Category } from "src/entity/category.entity";
import { CategoryRepository } from "src/repository/category.repository";
import { Connection } from "typeorm";

@Injectable()
export class CategoryService {
  private categoryRepository: CategoryRepository;

  constructor(private connection: Connection) {
    this.categoryRepository =
      this.connection.getCustomRepository(CategoryRepository);
  }

  async create(categoryDto: CategoryDto): Promise<Category> {
    return this.categoryRepository.createCategory(categoryDto);
  }

  async getCategory(id: number): Promise<Category> {
    return await this.categoryRepository.getCategory(id);
  }

  async getCategories(): Promise<Category[]> {
    return await this.categoryRepository.getCategories();
  }

  async updateCategory(id: number, dto: CategoryDto): Promise<Category> {
    return await this.categoryRepository.updateCategory(id, dto);
  }

  async deleteCategory(id: number): Promise<string> {
    return await this.categoryRepository.deleteCategory(id);
  }
}
