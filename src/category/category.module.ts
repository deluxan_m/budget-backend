import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { passportConfig } from "src/configs/passportConfig";
import { CategoryRepository } from "src/repository/category.repository";
import { CategoryController } from "./category.controller";
import { CategoryService } from "./category.service";

@Module({
  imports: [PassportModule.register(passportConfig)],
  controllers: [CategoryController],
  providers: [CategoryService, CategoryRepository],
})
export class CategoryModule {}
