import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { CategoryDto } from "src/dto/category.dto";
import { Category } from "src/entity/category.entity";
import { CategoryService } from "./category.service";

@Controller("category")
@UseGuards(AuthGuard())
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Post("/")
  create(@Body() categoryDto: CategoryDto): Promise<Category> {
    return this.categoryService.create(categoryDto);
  }

  @Get("/:id")
  getCategory(@Param("id") id: number): Promise<Category> {
    return this.categoryService.getCategory(id);
  }

  @Get("/")
  getCategories(): Promise<Category[]> {
    return this.categoryService.getCategories();
  }

  @Patch("/:id")
  updateCategory(
    @Param("id") id: number,
    @Body() categoryDto: CategoryDto
  ): Promise<Category> {
    return this.categoryService.updateCategory(id, categoryDto);
  }

  @Delete("/:id")
  deleteCategory(@Param("id") id: number): Promise<string> {
    return this.categoryService.deleteCategory(id);
  }
}
