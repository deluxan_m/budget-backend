import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { typeOrmConfig } from './configs/typeOrmConfig';
import { CategoryModule } from './category/category.module';

@Module({
  imports: [AuthModule, TypeOrmModule.forRoot(typeOrmConfig), CategoryModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
